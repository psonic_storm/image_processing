# differencing.py
# Description : detect differences between unmarked MCQ sheet and marked MCQ sheet
# Author : Paul Sebeikin
# Date-created : 31 August 2016
# Date-modified : 31 August 2016
try:
    print "[INFO] Importing Libraries..."
    import cv2 as cv  # OpenCV
    import numpy as np  # NumPy
    import argparse # argParse
    import imutils
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

def main():
	ap = argparse.ArgumentParser()
	ap.add_argument( "first_file", help="the source file")
	ap.add_argument( "second_file", help="the file on which to perform differencing")
	ap.add_argument( "-o", "--output", help="output path")
	args = ap.parse_args()

	# Read in image files
	img1 = cv.imread(args.first_file, 0) # source file
	img2 = cv.imread(args.second_file, 0) # file to be marked
	output = img2.copy()

	# Gaussian blur
	#cv.GaussianBlur(img1, (5,5), 0, img1)
	#cv.GaussianBlur(img2, (5,5), 0, img2)

	# Threshold image
	thresh1 = cv.adaptiveThreshold(img1, 255, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, 75, 10 )
	thresh2 = cv.adaptiveThreshold(img2, 255, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, 75, 10 )

	# Invert all bits
	cv.bitwise_not(thresh1, thresh1)
	cv.bitwise_not(thresh2, thresh2)

	#kernel = np.ones((5,5), np.uint8)
	#cv.erode(img2, kernel, img2, iterations = 1)

	cv.Canny(thresh2, 50, 150, thresh2, apertureSize = 5)
	
	lines = cv.HoughLinesP(thresh2, 1, np.pi/180, 100, 2, 20 )
	'''for rho, theta in lines[0]:
		a = np.cos(theta)
		b = np.sin(theta)
		x0 = a*rho
		y0 = b*rho
		x1 = int(x0 + 1000*(-b))
		y1 = int(y0 + 1000*(a))
		x2 = int(x0 - 1000*(-b))
		y2 = int(y0 - 1000*(a))
		cv.line(img2, (x1, y1), (x2, y2), (0,0,255),10)'''
	for line in lines:		
		(x1, y1, x2, y2) = line[0]
		cv.line(output, (x1, y1), (x2, y2), (0,255,0), 5)

	

	print "[INFO] : Number of lines detected = %i" % len(lines)

	'''circles = cv.HoughCircles(thresh2, cv.HOUGH_GRADIENT, 1, 9, param1=9, param2=7, minRadius=5, maxRadius=7)
	if circles is not None:
		circles = np.round(circles[0, :]).astype('int')
		print "Circles detected = %i" % len(circles)
		for (x, y, r) in circles:
			cv.circle(img2, (x,y), r, (0, 255, 0), 2)

	print "[INFO] : Number of circles detected = %i" % len(circles)'''

	img2 = imutils.resize(img2, width = 500)
	output = imutils.resize(output, width = 500)

	#cv.imshow("thresh2", thresh2)
	cv.imshow("img2", img2)
	cv.imshow("output", output)

	# Wait for user to press a key and then destroy all open windows
	cv.waitKey(0)
	cv.destroyAllWindows()

if __name__ == "__main__":
    main()