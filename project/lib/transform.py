# transform.py
# Description : various transformation functions for image processing
# Author : Paul Sebeikin
# Date-created : 7 September 2016
# Date-modified : 7 September 2016

import cv2 as cv  # OpenCV
import numpy as np  # NumPy
import imutils
import sys
sys.path.append("./lib")
import preprocessing
import geometry

def resize(image, width):
    return imutils.resize(image, width=width)

def perspectiveTransform(image, rotatepoints):
    w, h = image.shape[:2]
    reqLoc = np.float32([ [687, 22], [1087,22], [687, 142], [1087, 142] ])    
    matrix = cv.getPerspectiveTransform(rotatepoints, reqLoc)
    output = cv.warpPerspective(image, matrix, (w,h))
    return output

def affineTransform(image, rotatepoints):
    w, h = image.shape[:2]
    reqLoc = np.float32([ [687, 22], [1087,22], [687, 142] ])
    rotatepoints2D = np.float32( [ rotatepoints[0], rotatepoints[1], rotatepoints[2] ])
    matrix = cv.getAffineTransform(rotatepoints2D, reqLoc)
    output = cv.warpAffine(image, matrix, (w,h), cv.INTER_CUBIC)
    return output

def compute_skew(image):
    image = cv.bitwise_not(image)
    height, width = image.shape

    edges = cv.Canny(image, 150, 200, 3, 5)
    lines = cv.HoughLinesP(edges, 1, np.pi/180, 100, minLineLength=width / 2.0, maxLineGap=20)
    angle = 0.0
    nlines = lines.size
    for x1, y1, x2, y2 in lines[0]:
        angle += np.arctan2(x2 - x1, y2 - y1)
    return angle / nlines

def deskew(image, angle):
    image = cv.bitwise_not(image)
    non_zero_pixels = cv.findNonZero(image)
    center, wh, theta = cv.minAreaRect(non_zero_pixels)

    root_mat = cv.getRotationMatrix2D(center, angle, 1)
    cols, rows = image.shape
    rotated = cv.warpAffine(image, root_mat, (cols, rows), flags=cv.INTER_CUBIC)
    return cv.getRectSubPix(rotated, (cols, rows), center)

def four_point_transform(image, pts):
	rect = order_points(pts)
	(tl, tr, br, bl) = rect
 
	widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
	widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
	maxWidth = max(int(widthA), int(widthB))
 
	heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
	heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
	maxHeight = max(int(heightA), int(heightB))
 
	dst = np.array([
		[0, 0],
		[maxWidth - 1, 0],
		[maxWidth - 1, maxHeight - 1],
		[0, maxHeight - 1]], dtype = "float32")
 
	M = cv.getPerspectiveTransform(rect, dst)
	warped = cv.warpPerspective(image, M, (maxWidth, maxHeight))
 
	return warped

def order_points(pts):
	rect = np.zeros((4, 2), dtype = "float32")
 
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]
 
	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
 

	return rect

def rotate(image, point):
	x, y = point
	height, width = image.shape[:2]
	if x < width/2 and y > height/2: # bottom left
		return imutils.rotate(image, 180)
	else:
		# print 'No rotation'
		return image


