# preprocessing.py
# Description : various pre-processing functions for image processing project
# Author : Paul Sebeikin
# Date-created : 7 September 2016
# Date-modified : 7 September 2016

import cv2 as cv  # OpenCV
import numpy as np  # NumPy
from enum import Enum

kernel = np.ones((5,5), np.uint8)

class morphologytype(Enum):
    opening = 0
    closing = 1
    gradient = 2

class thresholdingtype(Enum):
    simple = 0
    adaptive = 1

def gaussianblur(image):    
    cv.GaussianBlur(image, (5,5), 0, image)

def erosion(image, iter):
    cv.erode(image, kernel, image, iterations = iter)

def canny(image):
    cv.Canny(image, 100,200, image)

def dilation(image, iter):
    cv.dilate(image, kernel, image, iterations = iter)

def morphology(image, type):
    if type == morphologytype.opening:
        cv.morphologyEx(image, cv.MORPH_OPEN, kernel, image)
    elif type == morphologytype.closing:
        cv.morphologyEx(image, cv.MORPH_CLOSE, kernel, image)
    elif type == morphologytype.gradient:
        cv.morphologyEx(image, cv.MORPH_GRADIENT, kernel, image)
def thresholding(image, type):
    if type == thresholdingtype.adaptive:
        cv.adaptiveThreshold(image, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 75, 10.0, image)
    elif type == thresholdingtype.simple:
		cv.threshold(image, 128, 255, cv.THRESH_BINARY, image)        

def inversion(image):
    cv.bitwise_not(image, image)