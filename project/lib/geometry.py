# geometry.py
# Description : various geometrical functions for image processing project
# Author : Paul Sebeikin
# Date-created : 7 September 2016
# Date-modified : 7 September 2016

import cv2 as cv  # OpenCV
import numpy as np  # NumPy
from enum import Enum

mcqbox = (180,200)

class templatetypes(Enum):
    single = 0
    multiple = 1

# Template matching methods for comparison in a list
methods = ['cv.TM_CCOEFF', 'cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR',
            'cv.TM_CCORR_NORMED', 'cv.TM_SQDIFF', 'cv.TM_SQDIFF_NORMED']

def templatematching(image, temp, meth, type):
    e1 = cv.getTickCount()

    # get global variables
    global methods

    rotatepoints = None

    # local variables
    method = eval(meth)
    rectangles = 0
    w, h = temp.shape[::-1]
    points = []

    # Apply template Matching
    res = cv.matchTemplate(image,temp,method)
    
    if type == templatetypes.single:
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
        # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
            top_left = min_loc        
        else:
            top_left = max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)        
        rotatepoints = np.float32([ top_left, [top_left[0] + w, top_left[1]], [top_left[0], top_left[1] + h], bottom_right])
        cv.rectangle(image,top_left, bottom_right, (0,128,0), 8)
        points.append(top_left)
        rectangles += 1

    elif type == templatetypes.multiple:
        threshold = 0.8
        loc = np.where( res >= threshold)
        prevtl, prevbr = None, None
        for pt in zip(*loc[::-1]):       
            if prevtl is None and prevbr is None: 
                prevtl = pt
                prevbr = (pt[0] + w, pt[1] + h)
            else:                     
                xi, yi = prevtl
                pi, qi = prevbr            
                x, y = pt
                p, q = (pt[0] + w, pt[1] + h)
                prevtl = pt
                prevbr = (pt[0] + w, pt[1] + h)
                if x in range(xi, xi + 5) or x in range(xi, xi - 5) or y in range(yi, yi + 5 ) or y in range(yi, yi - 5 ):
                    if p in range(pi, pi + 5) or p in range(pi, pi - 5) or q in range(qi, qi + 5 ) or q in range(qi, qi - 5 ):
                        continue       
            cv.rectangle(image, pt, (pt[0] + w, pt[1] + h), (0,128,0), 8)   
            # print "[INFO] Drawing rectangle at %s,%s" % (pt, (pt[0] + w, pt[1] + h) )         
            points.append(pt)
            rectangles += 1        

    
    # print "[INFO] Rectangles drawn : %i" % rectangles    
    e2 = cv.getTickCount()
    time = (e2 - e1)/ cv.getTickFrequency()
    # print "[SUCCESS] Template matching operation completed successfully in %i seconds" % time
    return points, rotatepoints

    # # display results
    # plt.subplot(141),plt.imshow(res,cmap = 'gray')
    # plt.title('Match Result'), plt.xticks([]), plt.yticks([])
    # plt.subplot(142),plt.imshow(image,cmap = 'gray')
    # plt.title('Output Image'), plt.xticks([]), plt.yticks([])
    # plt.subplot(143),plt.imshow(src,cmap = 'gray')
    # plt.title('Source Image'), plt.xticks([]), plt.yticks([])
    # plt.subplot(144),plt.imshow(template,cmap = 'gray')
    # plt.title('Template'), plt.xticks([]), plt.yticks([])
    # plt.suptitle('cv.TM_COEFF_NORMED')
    # plt.show(

def drawlines(image, dest, minLineLength = 100, maxLineGap = 10):
    angle = 0
    e1 = cv.getTickCount()
    lines = cv.HoughLinesP(image, 1, np.pi/180, 100, minLineLength, maxLineGap )    

    for line in lines:      
        (x1, y1, x2, y2) = line[0]
        cv.line(dest, (x1, y1), (x2, y2), (0,128,0), 2)
        angle += np.arctan2(float(y2 - y1), float(x2 - x1));
    angle /= len(lines)
    return angl

def findcircles(image): 
    # inversion(image)
    #thresholding(image, thresholdingtype.adaptive)
    #erosion(image, 1)
    centers = []
    circles = cv.HoughCircles(image, cv.HOUGH_GRADIENT, 5, 25, param1=50, param2=30, minRadius=0, maxRadius=0)
    if circles is not None:
        circles = np.round(circles[0, :]).astype('int')
        # print "Circles detected = %i" % len(circles)
        for (x, y, r) in circles:
            cv.circle(image, (x,y), r, (255, 255, 255), 3)
            centers.append((x,y))
        print "[INFO] : Number of circles detected = %i" % len(circles)        
    else:
        print "[INFO] : No circles detected."
    return centers

def findcontours(image):
    global mcqbox     
    grid = 5
    blocksz = (mcqbox[0]/grid, mcqbox[1]/grid) # 34 x 40

    # xOffset, yOffset = 0,0
    # rect_locations = []
    # for i in range(25):
    #     tl = (blocksz[0] * xOffset, blocksz[1] * yOffset)
    #     br = (tl[0] + blocksz[0], tl[1] + blocksz[1])
    #     rect_locations.append( [tl,br] )
    #     xOffset += 1
    #     if (xOffset % 5 is 0):
    #         xOffset = 0 
    #         yOffset += 1

    # print rect_locations
    # local variables
    drawn = 0
    centers = []
    # image, contours, hierarchy = cv.findContours(image, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    #image, contours, hierarchy = cv.findContours(image, cv.RETR_TREE, cv.CHAIN_APPROX_TC89_L1)    
    image, contours, hierarchy = cv.findContours(image, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    for i in range(len(contours)):
        if hierarchy[0][i][3] == -1: # no parent hierarchy then skip because it is likely a border
            continue
        cnt = contours[i]      
        (x,y), radius = cv.minEnclosingCircle(cnt)
        center = (int(x), int(y))
        radius = int(radius)        
        area = cv.contourArea(cnt)
        if radius < 10 or radius > 22: # disregard large or small circles.  
            continue

        # if area < 350 or area > 950:
        # 	continue

        # if len(cnt) < 5:
        # 	continue
        
        # for i in range(len(rect_locations)):
        #     tl, br = rect_locations[i]
        #     if tl[0] < center[0] and tl[1] < center[1] and center[0] < br[0] and center[1] < br[1]:
        #         cv.rectangle(image, rect_locations[i][0], rect_locations[i][1], (255,255,255), 1)
        #         # cv.fillPoly(image, rect_locations[i], (255, 255, 255))        
        #         print "Found point in rectange: %s" % rect_locations[i]
        #         drawn += 1
        #         break

        # rect = cv.minAreaRect(cnt)
        # box = cv.boxPoints(rect)
        # box = np.int0(box)

        # x,y,w,h = cv.boundingRect(cnt)
        # cv.rectangle(image, (x,y), (x+w, y+h), (255,255,255), 2)

        # approximate a rectangle
        # epsilon = 0.1 * cv.arcLength(cnt, True)
        # approx = cv.approxPolyDP(cnt, epsilon, True)

        # draw circle
        cv.circle(image, center, radius, (255,255,255), 1)
        
        # draw elipse
     #    ellipse = cv.fitEllipse(cnt)
     #    print ellipse.center
    	# cv.ellipse(image, ellipse, (255,255,255), 2)
        
    	# # draw rectangle
    	# cv.drawContours(image, [box], 0, (255,255,255), 1)
     #    cv.fillPoly(image, [box], (255, 255, 255))        
        centers.append(center)
        
    # print "[INFO] Contours drawn: %i" % drawn
    return centers

def findcorners(image):
	blocksz = 2
	ksize = 3
	k = 0.04
	image = np.float32(image)
	dest = cv.cornerHarris(image, blocksz, ksize, k)
	return dest
