# mcq.py
# Description : detect mcq answers and save them to a csv file
# Author : Paul Sebeikin
# Date-created : 22 August 2016
# Date-modified : 7 September 2016

try:
    print "[INFO] Importing Libraries..."
    import cv2 as cv  # OpenCV
    import numpy as np  # NumPy
    import argparse # argParse
    import sys, os # file handling modules
    sys.path.append("./lib")
    import transform
    import geometry
    import preprocessing
    import file_io
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

# Global Variables
rotatepoints = None

answerOffset = 0

def findmcq(image, points):

    mcqbox = geometry.mcqbox

    w, h = image.shape[:2]
    offsetX, offsetY = 0,0    
    boxes = []
    box_locations = []

    img_copy = image.copy()


    for i in range(12):
        if i == 6:
            offsetX = 314
            offsetY = 0
        if points[0][0] < w/2 and points[0][1] < h/2 : # top left corner
            x1 = points[0][0] + 465 + offsetX
            y1 = points[0][1] + 75 + offsetY
            x2 = points[0][0] + 465 + offsetX + mcqbox[0]
            y2 = points[0][1] + 75 + offsetY + mcqbox[1]
            # cv.rectangle(image, (x1,y1), (x2,y2), (0,128,0), 4)
            box_locations.append( np.float32([ [ x1, y1] , [ x2, y1], [y2, x2], [ x1, y2] ]) )
            boxes.append(img_copy[y1:y2, x1:x2])
            # print "top left corner"
        elif points[0][0] > w/2 and points[0][1] < h/2: # top right corner
            x1 = points[0][0] - 600 + offsetX
            y1 = points[0][1] + 75 + offsetY
            x2 = points[0][0] - 600 + offsetX + mcqbox[0]
            y2 = points[0][1] + 75 + offsetY + mcqbox[1]
            # cv.rectangle(image, (x1,y1), (x2,y2), (0,128,0), 4)
            box_locations.append( np.float32([ [ x1, y1] , [ x2, y1], [y2, x2], [ x1, y2] ]) )
            boxes.append(img_copy[y1:y2, x1:x2])
            # print "top right corner"
        elif points[0][0] < w/2 and points[0][1] > h/2: # bottom left corner
            x1 = points[0][0] + 455 + offsetX
            y1 = points[0][1] - 1340 + offsetY
            x2 = points[0][0] + 455 + mcqbox[0]
            y2 = points[0][1] - 1340 + mcqbox[1]
            # cv.rectangle(image, (x1,y1), (x2,y2), (0,128,0), 4)
            box_locations.append( np.float32([ [ x1, y1] , [ x2, y1], [y2, x2], [ x1, y2] ]) )
            boxes.append(img_copy[y1:y2, x1:x2])
            # print "bottom left corner"
        elif points[0][0] > w/2 and points[0][1] > h/2: # bottom right corner
            x1 = points[0][0] - 605 + offsetX
            y1 = points[0][1] - 1337 + offsetY
            x2 = points[0][0] - 605 + mcqbox[0]
            y2 = points[0][1] - 1337 + mcqbox[1]
            # cv.rectangle(image, (x1,y1), (x2,y2), (0,128,0), 4)
            box_locations.append( np.float32([ [ x1, y1] , [ x2, y1], [y2, x2], [ x1, y2] ]) )
            boxes.append(img_copy[y1:y2, x1:x2])
            # print "bottom right corner"
        offsetY += 218  
    return boxes, box_locations

def getscore(center):
    global answerOffset # mcqbox = 170 x 200
    mcqbox = geometry.mcqbox
    grid = 5
    blocksz = (mcqbox[0]/grid, mcqbox[1]/grid) # 34 x 40

    # compute position
    row = int(center[1] / blocksz[1]) + 1 + answerOffset
    columnX = int(center[0] / blocksz[0])
    columnY = int(center[1] / blocksz[1])

    # print center
    # print "Got row :%i and column %i" % (row,columnX)

    if columnX == 0:
        return row, 'a' 
    elif columnX == 1:
        return row, 'b'
    elif columnX == 2:  
        return row, 'c'
    elif columnX == 3:
        return row, 'd'
    elif columnX == 4:
        return row, 'e'
    else:
        return row, 'n/a'

def findstudnum(image, points):
    x1 = points[0][0] 
    y1 = points[0][1] + 110 
    x2 = points[0][0] + 280
    y2 = points[0][1] + 110 + 1010
    #cv.rectangle(image, (x1,y1), (x2,y2), (0,128,0), 4)
    res = image.copy()
    return res[y1:y2, x1:x2]

def findtasknum(image, points):
    x1 = points[0][0] 
    y1 = points[0][1] + 50 
    x2 = points[0][0] + 100
    y2 = points[0][1] + 50 + 400
    #cv.rectangle(image, (x1,y1), (x2,y2), (0,128,0), 4)    
    res = image.copy()
    return res[y1:y2, x1:x2]

def parsestudnum(centers):
    # if len(centers) > 10:
    #     print "[ERROR] : Student number could not be parsed because %i circles were detected. 9 expected" % len(centers)
    #     return None
    # else:    
    box = 270, 995
    grid = 7,26
    blocksz = (box[0]/grid[0], box[1]/grid[1])    

    student_number = "g"

    for i in range(len(centers)):
        if i == 2:
            row = int(centers[i][1] / blocksz[1])                 
            student_number += chr(row + 65)   
        elif centers[i][1] >= 400: # task number                
            continue
        else:
            row = int(centers[i][1] / blocksz[0])
            student_number += str(row)

    return student_number   

def parsetasknum(centers):
    # if len(centers) > 2:
    #     print "[ERROR] : Task number could not be parsed because %i circles were detected. 2 expected" % len(centers)
    #     return None
    # else:    
    box = 75, 385
    grid = 2, 10
    blocksz = (box[0]/grid[0], box[1]/grid[1])    

    # print centers

    task_number = ""

    for i in range(len(centers)):            
        row = int(centers[i][1] / blocksz[1])
        task_number += str(row)
    return task_number

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-f", "--file", type=str, default=None, help="the source file")
    ap.add_argument( "-o", "--output", type=str, help="output path")
    ap.add_argument( "-d", "--dir", type=str, help="directory in which to find png files")
    ap.add_argument( "-r", "--resolution", type=int, default=600, help="the resolution of the input pdf file (in dpi)")
    args = vars(ap.parse_args())

    # GLOBAL VARIABLES
    global answerOffset

    # LOCAL VARIABLES
    student_number, task_number = None, None
    path = args["output"]    
    images = []
    filesprocessed = 0

    # Read in basic mcq sheet
    mcq_model = cv.imread("./img/basic.png", 0)

    # Read om template for mcq box
    mcq = cv.imread("./img/mcq.png", 0)

    # Read in template for template matching and make copy for pre-processing
    template = cv.imread("./img/corner_circle.png", 0)

    # Read in department logo sheet in greyscale
    logo = cv.imread("./img/dept_logo.png", 0)

    # Read in student number label in greyscale
    stud_num = cv.imread("./img/stud_num.png", 0)

    # Read in task number label in greyscale
    task = cv.imread("./img/task.png", 0)

    preprocessing.gaussianblur(template)    
    preprocessing.gaussianblur(stud_num) 
    preprocessing.gaussianblur(task)
    preprocessing.gaussianblur(logo)
    preprocessing.gaussianblur(mcq)
    preprocessing.gaussianblur(mcq_model)

    if args["file"] is None:
        for file in os.listdir(args["dir"]):
            images.append(args["dir"] + file)
    else:
        images = file_io.convert_pdf_to_png(args["file"])

    for i in range(len(images)):        
        print "[INFO] Processing image at '%s'" % images[i]
        answerOffset = 0
        scores = []

        # Read in MCQ sheet in greyscale
        img_gray = cv.imread(images[i], 0)
        # img_gray = transform.resize(img_gray, 800)
        
        preprocessing.gaussianblur(img_gray)

        # CHECK FOR ROTATION
        temp = img_gray.copy()
        
        # cv.imshow("before_rotate", temp)
        # cv.waitKey(0)

        points, rotatepoints = geometry.templatematching(temp, logo, geometry.methods[1], int(geometry.templatetypes.single)) # find department logo
        # print "[INFO] Rotate points %s" % points
        main_img = transform.rotate(img_gray, points[0])

        # main_img = transform.resize(main_img, 500)
        # cv.imshow('after_rotation', main_img)
        # cv.waitKey(0)

        # deskewed_image = transform.deskew(main_img.copy(), transform.compute_skew(main_img.copy()))
        # deskewed_image = transform.resize(deskewed_image, 800)

        # cv.imshow("deskewed mcq", deskewed_image)
        # cv.waitKey(0)
        # cv.destroyAllWindows()

        stud_num_copy = main_img.copy()
        task_num_copy = main_img.copy()
        mcq_copy = main_img.copy()

        # GET STUDENT NUMBER
        points, _ = geometry.templatematching(stud_num_copy, stud_num, geometry.methods[1], int(geometry.templatetypes.single)) # find student number label        
        stud_img = findstudnum(stud_num_copy, points)      
        print "[INFO] Student number points %s" % points
        # cv.imshow('studnum', stud_img)
        # cv.waitKey(0)        

        preprocessing.thresholding(stud_img, preprocessing.thresholdingtype.adaptive)
        preprocessing.morphology(stud_img, preprocessing.morphologytype.closing)
        preprocessing.erosion(stud_img, 1)

        # cv.imshow("before_finding_student_number", stud_img)
        # cv.waitKey(0)
        # cv.destroyAllWindows()

        locations = geometry.findcontours(stud_img)    

        # cv.imshow('after_finding_student_number', stud_img)
        # cv.waitKey(0)
        # cv.destroyAllWindows()       
        
        # locations = geometry.findcircles(stud_img)
        locations = sorted(locations, key=lambda k: [k[0], k[1]])
        student_number = parsestudnum(locations)
        print "[INFO] Found student number : '%s'" % student_number
        
        # GET TASK NUMBER
        points, _ = geometry.templatematching(task_num_copy, task, geometry.methods[1], int(geometry.templatetypes.single)) # find task number label    
        task_img = findtasknum(task_num_copy, points)
        preprocessing.thresholding(task_img, preprocessing.thresholdingtype.adaptive)
        preprocessing.morphology(task_img, preprocessing.morphologytype.closing)
        locations = geometry.findcontours(task_img)
        locations = sorted(locations, key=lambda k: [k[0], k[1]])
        task_number = parsetasknum(locations)
        print "[INFO] Found task number : '%s'" % task_number

        # GET MCQ BOXES
        points, _ = geometry.templatematching(mcq_copy, template, geometry.methods[1], int(geometry.templatetypes.multiple)) # find corners
        # cv.imshow("mcq box found", mcq_copy)
        # cv.waitKey(0)
        # cv.destroyAllWindows()

        if len(points) < 1:
            print "[ERROR] Processing file '%s'" % images[i]
            continue        
        boxes, box_locations = findmcq(mcq_copy, points) # find mcqboxes    
        for j in range(len(boxes)):
            box= boxes[j]

            # cv.imshow("mcq box before preprocessing", box)
            # cv.waitKey(0)
            # cv.destroyAllWindows()

            # pre processing
            preprocessing.thresholding(box, preprocessing.thresholdingtype.adaptive)            
            preprocessing.morphology(box, preprocessing.morphologytype.opening)                
            preprocessing.morphology(box, preprocessing.morphologytype.closing)

            # cv.imshow("mcq box before contours", box)
            # cv.waitKey(0)
            # cv.destroyAllWindows()

            locations = geometry.findcontours(box)
            # locations = geometry.findcircles(box)
            locations = sorted(locations, key=lambda k: [k[0], k[1]])
            for loc in range(len(locations)):
                score = getscore(locations[loc])    
                matches = [s for s in scores if s[0] == score[0]]
                if len(matches) > 0:
                    indx = scores.index(matches[0])
                    q,a = scores[indx]
                    a += score[1]
                    scores[indx] = q,a
                    continue                
                scores.append(score)
            answerOffset += 5     
            # cv.imwrite("./mcq.png", box)
            # cv.imshow("mcq box at end", box)
            # cv.waitKey(0)
            # cv.destroyAllWindows()
        
        print "[INFO] Found %i MCQ boxes and processed %i scores" % (len(boxes), len(scores))    
        outputfile = path + "scores-%s.csv" % images[i][7:-4]
        file_io.writefile(student_number, task_number, scores, outputfile)
        filesprocessed += 1
        cv.destroyAllWindows()

if __name__ == "__main__":
    main()
