# frame_diff.py
# Description : performs a frame difference operation on a set of PPM images in a specified directory
# Author : Paul Sebeikin
# Date-created : 21 July 2016
# Date-modified : 12 September 2016
try:
    print "[INFO] Importing Libraries..."
    import sys, os, math, argparse
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

# Global variables
ppmPixels = []

class Pixel:
    def __init__(self, r, g, b):
        self.red = r
        self.green = g
        self.blue = b

def readFile(filename):
    return open(filename)

def getHeader(file):
    global ppmPixels
    linesRead = 0
    temp = []
    for line in file:
        if linesRead < 4:
            temp.append(line)
        else:
            ppmPixels.append(line)
        linesRead += 1
    return temp

def getPixels(file):
    linesRead = 0
    pixels = list()
    for line in file:
        if linesRead == 0:
            p = Pixel(0, 0, 0)
            p.red = line
        elif linesRead  == 1:
            p.green = line
        elif linesRead == 2:
            p.blue = line
            pixels.append(p)
            linesRead = -1
        linesRead += 1
    return pixels


def nearest_neighbour(pixels, w1, h1, w2, h2):
    newPixels = [None] * (w2*h2)

    # work out scaling ratios
    x_ratio = w1 / float(w2)
    y_ratio = h1 / float(h2)

    for i in range(h2):
        for j in range(w2):
            px = int(math.floor(j * x_ratio))
            py = int(math.floor(i * y_ratio))
            indxA = (i * w2) + j
            indxB = (py * int(w1)) + px
            newPixels[indxA] = pixels[indxB]
    return newPixels

def interpolate(pixels, w1, h1, w2, h2):
    # initialize new image pixel list
    newPixels = [None] * (w2*h2)

    # work out scaling ratios
    x_ratio = (w1-1) / float(w2)
    y_ratio = (h1-1) / float(h2)

    offset = 0
    for i in range(h2):
        for j in range(w2):
            x = int(x_ratio * j)
            y = int(y_ratio * i)
            x_diff = int((x_ratio * j) - x)
            y_diff = int((y_ratio * i) - y)
            index = (y * w1) + x

            A = pixels[index]
            B = pixels[index+1]
            C = pixels[index+w1]
            D = pixels[index+w1+1]

            Ax = A.red * (1-x_diff) * (1-y_diff)
            Bx = B.red * x_diff * (1-y_diff)
            Cx = C.red * y_diff * (1-x_diff)
            Dx = D.red * (x_diff * y_diff)
            red = int(Ax + Bx + Cx + Dx)

            Ax = A.green * (1-x_diff) * (1-y_diff)
            Bx = B.green * x_diff * (1-y_diff)
            Cx = C.green * y_diff * (1-x_diff)
            Dx = D.green * (x_diff * y_diff)
            green = int(Ax + Bx + Cx + Dx)

            Ax = A.blue * (1-x_diff) * (1-y_diff)
            Bx = B.blue * x_diff * (1-y_diff)
            Cx = C.blue * y_diff * (1-x_diff)
            Dx = D.blue * (x_diff * y_diff)
            blue = int(Ax + Bx + Cx + Dx)

            newPixels[offset] = Pixel(red, green, blue)

            offset += 1
    return newPixels

def writeFile(contents, path):
    f = open(path, 'w')
    for i in contents:
        if str(i)[-1:] == '\n':            
            f.write(str(i))
        else:            
            f.write(str(i) + '\n')
    f.close()
    print('File saved to: %s' % path)

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-f", "--file", type=str, help="the file on which the operation should be performed" )
    ap.add_argument( "-o", "--output", type=str, help="output path")
    ap.add_argument( "-a", "--algorithm", type=int, default= 0, help="the algorithm to execute : 0 - nearest neighbour 1 - interpolation")
    ap.add_argument( "-l", "--height", type=int, help="the new height of the scaled image")
    ap.add_argument( "-w", "--width", type=int, help="the new width of the scaled image")
    args = vars(ap.parse_args())

    # get file contents
    file = readFile(args["file"])
    temp = getHeader(file)
    pixels = getPixels(ppmPixels)

    # get original dimensions
    dimensions = temp[2].split(' ')
    
    # original image size
    w1 = int(dimensions[0])
    h1 = int(dimensions[1])

    # scaled image size
    w2 = args["width"]
    h2 = args["height"]

    output_path = args["output"]

    linesRead = 0

    algorithm = args["algorithm"]    

    temp[2] = str(w2) + ' ' + str(h2) + '\n' # add new size to PPM file

    if algorithm == 0:
        newPixels = nearest_neighbour(pixels, w1, h1, w2, h2)    
        output = output_path + 'scale_nearest_neighbour_' + str(w2) + 'x' + str(h2) + '.ppm'
    elif algorithm == 1:
        newPixels = interpolate(pixels, w1, h1, w2, h2)
        output = output_path + 'scale_interpolation_' + str(w2) + 'x' + str(h2) + '.ppm'

    for i in newPixels:
        temp.append(i.red)
        temp.append(i.green)
        temp.append(i.blue)    

    writeFile(temp, output)

if __name__ == "__main__":
    main()
