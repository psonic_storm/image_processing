# frame_diff.py
# Description : performs a frame difference operation on a set of PPM images in a specified directory
# Author : Paul Sebeikin
# Date-created : 27 July 2016
# Date-modified : 12 September 2016

try:
    print "[INFO] Importing Libraries..."
    import argparse
    import time
    import numpy as np
    import imutils
    import os
    import math
    import cv2
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

# Global variables
firstFrame = None
threshold = None
fps = None
mode = None
operation = None

def difference(frame1, frame2):
    result = np.zeros( (len(frame1), len(frame1[0]), 3) )
    for i in range(len(frame1)):
        for j in range(len(frame1[i])):
            r,g,b = 0,0,0
            if frame1[i][j][0] != frame2[i][j][0] or frame1[i][j][1] != frame2[i][j][1] or frame1[i][j][2] != frame2[i][j][2]:
                r,g,b = 255, 255, 255

            # r = math.fabs(frame2[i][j][0] - frame1[i][j][0])
            # g = math.fabs(frame2[i][j][1] - frame1[i][j][1])
            # b = math.fabs(frame2[i][j][2] - frame1[i][j][2])
            # if frame1[i][j][0] != frame2[i][j][0] or  or frame1[i][j][2] != frame2[i][j][2]:
            #     print frame1[i][j], frame2[i][j]
            # print r,g,b
            result[i][j] = np.array( [r,g,b] )
    return result

def subtraction(frame, background, threshold):
    result = np.zeros( (len(frame), len(frame[0]), 3) )
    # frame = difference(background, frame)
    # cv2.imshow("frame", frame)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    for i in range(len(frame)):
        for j in range(len(frame[i])):
            if math.fabs(frame[i][j][0] - background[i][j][0]) >= threshold or math.fabs(frame[i][j][1] - background[i][j][1]) >= threshold or math.fabs(frame[i][j][2] - background[i][j][2]) >= threshold:
                    result[i][j] = np.array([255,255,255])
            else:
                result[i][j] = np.array([0, 0, 0]) # no difference
    return result

def running_avg(frame, background, threshold):
    a = 0.05
    result = np.zeros( (len(frame), len(frame[0]), 3) )
    for i in range(len(frame)):
        for j in range(len(frame[i])):
            r = (a * frame[i][j][0]) + (1 - a) * background[i][j][0]
            g = (a * frame[i][j][1]) + (1 - a) * background[i][j][1]
            b = (a * frame[i][j][2]) + (1 - a) * background[i][j][2]
            result[i][j] = np.array( [avg,avg,avg] )
    return result

def parsefile(file, output):
    output.append(file.next()) # P3
    # output.append(file.next()) # Comment
    output.append(file.next()) # dimensions
    output.append(file.next()) # color threshold (255)

    dimensions = output[1].split(' ') # get width and height
    width = int(dimensions[0])
    height = int(dimensions[1])

    pixels = np.zeros( (width,height, 3) ) # create 2d array

    line = file.next()
    lineSplit = line.split(' ')


    if len(lineSplit) < 2:
        for i in range(len(pixels)):
            for j in range(len(pixels[i])):
                for k in range(len(pixels[i][j])):
                    if i == 0 & j == 0 & k == 0:
                        pixels[i][j][k] = int(line)
                    else:
                        pixels[i][j][k] = int(file.next())
    else:
        fileLineCounter = 0
        for i in range(len(pixels)):
            for j in range(len(pixels[i])):
                for k in range(len(pixels[i][j])):
                    if lineSplit[fileLineCounter] == '\n':
                        continue
                    pixels[i][j][k] = int(lineSplit[fileLineCounter])
                    fileLineCounter += 1
    return pixels

def writefile(contents, path):
    f = open(path, 'w')
    for i in contents:
        if str(i)[-1:] == '\n':
            f.write(str(i))
        else:
            f.write(str(i) + ' ')
    f.close()
    print('[INFO] File saved to: %s' % path)

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-t", "--threshold", type=int, default=50, help="threshold to apply to the background subtraction algorithm")
    ap.add_argument( "-d", "--dir", type=str, help="the directory in which the set of images are contained")
    ap.add_argument( "-o", "--output", type=str, help="output path")
    ap.add_argument( "-b", "--background", type=str, help="the path of the background file")
    ap.add_argument( "-a", "--algorithm", type=int, default= 0, help="the algorithm to use : 0 - background subtraction; 1 - running average")
    args = vars(ap.parse_args())

    global firstFrame, threshold
    threshold = args['threshold']
    algorithm = args["algorithm"]
    output_path = args["output"]

    tmp  = []
    background = parsefile(open(args["background"]), tmp)
    filePaths = []
    for file in os.listdir(args["dir"]):
        filePaths.append(args["dir"] + '/' + file)

    if algorithm is 0:
        print('[INFO] Executing background subtraction algorithm')

        for i in range(len(filePaths)):
            output = []
            pixels = parsefile(open(filePaths[i]), output )
            result = subtraction(pixels, background, threshold)

            filename = os.path.splitext(filePaths[i])
            image_path = output_path + 'background_' + str(i) + '.ppm'

            for p in range(len(result)):
                for q in range(len(result[p])):
                    for r in range(len(result[p][q])):
                        output.append(int(result[p][q][r]))
            writefile(output, image_path)
    elif algorithm is 1:
        print('[INFO] Executing running average subtraction algorithm')

        for i in range(len(filePaths)):
            output = []
            pixels = parsefile(open(filePaths[i]), output )
            result = running_avg(pixels, background, threshold)

            filename = os.path.splitext(filePaths[i])
            image_path = output_path + 'running_avg_' + str(i) + '.ppm'

            for p in range(len(result)):
                for q in range(len(result[p])):
                    for r in range(len(result[p][q])):
                        output.append(int(result[p][q][r]))
            writefile(output, image_path)
        print('[INFO] Running average algorithm completed successfully.')

if __name__ == "__main__":
	main()
