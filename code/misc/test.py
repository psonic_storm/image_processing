import numpy as np

class Pixel:
    def __init__(self, r, g, b):
        self.red = r
        self.green = g
        self.blue = b

a = np.zeros( (2, 2, 3) )
a[0][0][0] = 1
a[0][0][1] = 2
a[0][0][2] = 3
a[0][1][0] = 3
a[0][1][1] = 2
a[0][1][2] = 1

print(a)
