# jc_sample_code.py
# Description : Sample Code created during JC Guest Image Processing Lecture
# Author : Paul Sebeikin (JC Bailey)
# Date-created : 12 August 2016
# Date-modified: 12 August 2016

import cv2 as cv

def edge_detection():
	ndaContours, heirarchy = cv.findContours(
		grayscale_image.copy(), # use copy, because this function overrides the original image
		cv2.RETR_EXTERNAL, # there are different algorithms, this one only found the outside contours.
		cv2.CHAIN_APPROX_SIMPLE) # determines how many points the contour makes

def draw_rectangle():
	Rectangle(colour_image, topleft_coordinates, bottomright_coordinates, BGR_colours, line_thickness) 
	# giving a line thickness of -1 paintbucket fills the entire area

	cp = image.shape[1] / 2, image.shape[0] / 2 # (600, 480) height x width
	Circle(colour_image, centerpoint_coordinates, radius, BGR_colours, line_thickness)

	# Contours
	for (int i = 0; i < contours_array_size; i++):
		drawContours(colour_image, contour_array, i, BGR_colour, line_thickness)

	# Polygon
	array_of_points = [ [(x1, y1), (x2, y2), (x3, y3) ... (xn, yn) ]]

def onMouseLeftClick(event, x, y, flags, param):
	if event == cv2.EVENT_LBUTTONDOWN: # interrupt for left mouse button press
		# do something
		print x, y

window = cv.namedWindow("image")
cv.setMouseCallback("image", onMouseLeftClick) # bind mouse listener to the window and event function.

