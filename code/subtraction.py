# import packages
import cv2
import argparse
import time
import numpy as np
import imutils
import os

# Global variables
firstFrame = None
threshold = None
fps = None
mode = None
operation = None

def subtraction(frame, background, threshold):
    result = np.zeros( (len(frame), len(frame[0]), 3) )
    for i in range(len(frame)):
        for j in range(len(frame[i])):
            if (frame[i][j][0] - background[i][j][0] >= threshold) or (frame[i][j][1] - background[i][j][1] >= threshold) or (frame[i][j][2] - background[i][j][2] > threshold):
                    result[i][j] = np.array([255,255,255])
            else:
                result[i][j] = np.array([0, 0, 0]) # no difference
    return result

def parsefile(file, output):
    output.append(file.next()) # P3
    output.append(file.next()) # dimensions
    output.append(file.next()) # color threshold (255)

    dimensions = output[1].split(' ') # get width and height
    width = int(dimensions[0])
    height = int(dimensions[1])

    pixels = np.zeros( (width,height, 3) ) # create 2d array

    line = file.next()
    lineSplit = line.split(' ')

    if len(lineSplit) < 1:
        for i in range(len(pixels)):
            for j in range(len(pixels[i])):
                for k in range(len(pixels[i][j])):
                    if i == 0 & j == 0 & k == 0:
                        pixels[i][j][k] = int(line)
                    else:
                        pixels[i][j][k] = int(file.next())
    else:
        fileLineCounter = 0
        for i in range(len(pixels)):
            for j in range(len(pixels[i])):
                for k in range(len(pixels[i][j])):
                    pixels[i][j][k] = int(lineSplit[fileLineCounter])
                    fileLineCounter += 1
    return pixels

def writefile(contents, path):
    f = open(path, 'w')
    for i in contents:
        if str(i)[-1:] == '\n':
            f.write(str(i))
        else:
            f.write(str(i) + ' ')
    f.close()
    print('File saved to: %s' % path)

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-v", "--video", help="path to the video file")
    ap.add_argument( "-m", "--mode", type=int, default=2, help="toggle mode : 1 - set of images; 2 - video stream")
    ap.add_argument( "-t", "--threshold", type=int, default=50, help="threshold to apply to the background subtraction algorithm")
    ap.add_argument( "-f", "--fps", type=int, default=20, help="the frame rate per second for the video stream")
    ap.add_argument( "-d", "--dir", type=str, help="the directory in which the set of images are contained")
    ap.add_argument( "-b", "--background", type=str, help="the path of the background file")
    ap.add_argument( "-o", "--operation", type=int, default= 0, help="the operation to execute : 0 - background subtraction; 1 - frame difference")
    args = vars(ap.parse_args())

    global firstFrame, threshold, fps
    threshold = args['threshold']
    fps = args["fps"]
    mode = args["mode"]
    operation = args["operation"]

    if mode is 1:
        if operation is 0:
            print('[INFO] : Executing background subtraction operation in mode 1 - set of images')
            tmp  = []
            background = parsefile(open(args["background"]), tmp)
            filePaths = []
            for file in os.listdir(args["dir"]):
                filePaths.append(args["dir"] + '/' + file)
            for i in range(len(filePaths)):
                output = []
                pixels = parsefile(open(filePaths[i]), output )
                result = subtraction(pixels, background, threshold)

                filename = os.path.splitext(filePaths[i])
                outPath = './background_' + str(i) + filename[1]

                for p in range(len(result)):
                    for q in range(len(result[p])):
                        for r in range(len(result[p][q])):
                            output.append(int(result[p][q][r]))
                writefile(output, outPath)
        if operation is 1:
            print('[INFO] : Executing frame difference operation in mode 1 - set of images')
            filePaths = []
            for file in os.listdir(args["dir"]):
                filePaths.append(args["dir"] + '/' + file)
            i = 0
            while i < len(filePaths):
                output, tmp = ([], [])
                frame = parsefile(open(filePaths[i]), output )
                try:
                    next_frame = parsefile(open(filePaths[i+1]), tmp )
                except:
                    print("[INFO] : Operation completed.")
                    break
                result = subtraction(next_frame, frame, threshold)

                filename = os.path.splitext(filePaths[i])
                outPath = './frame_diff' + str(i) + filename[1]

                for p in range(len(result)):
                    for q in range(len(result[p])):
                        for r in range(len(result[p][q])):
                            output.append(int(result[p][q][r]))
                writefile(output, outPath)
                i += 2
    elif mode is 2:
        print('[INFO] : Executing operation in mode 2 - video stream')
        if args.get("video", None) is None:
            camera = cv2.VideoCapture(0)
            camera.set(5, fps) # 5 = CV_CAP_PROP_FPS

            print('[INFO] : Streaming from built-in camera')
        else:
            camera = cv2.VideoCapture(args["video"])
            camera.set(5, fps) # 5 = CV_CAP_PROP_FPS
            print('[INFO] : Streaming from network path : %s' % args["video"])

        print('[INFO] : Warming up the camera...')
        time.sleep(2.0)

        while(True):
            ret, frame = camera.read()
            if ret is None:
                break
            else:
                frame = imutils.resize(frame, width=300)
                if firstFrame is None:
                    firstFrame = frame

                result = subtraction(frame, firstFrame, threshold)
                cv2.imshow('Video', result)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

        camera.release()
        cv2.destroyAllWindows()

if __name__ == "__main__":
	main()
