# encrypt.py
# Description : encrypt a secret message in an image file.
# Author : Paul Sebeikin
# Date-created : 15 August 2016
# Date-modified : 112 September 2016

try:
    print "[INFO] Importing Libraries..."
    import argparse, numpy as np, hashlib
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

encrypted_pwd = None
minSz = 1
maxSz = 1000
pwdMinSz = 10
pwdMaxSz = 30

def writefile(contents, path):
    f = open(path, 'w')
    for i in contents:
        if str(i)[-1:] == '\n':            
            f.write(str(i))
        else:            
            f.write(str(i) + '\n')
    f.close()

def parsefile(file, output):
    global encrypted_pwd
    headerlines = 4
    pixels = []
    linecounter = 0

    for f in file:     
        if linecounter < headerlines or str(f[0]) == "#" :
            output.append(f)            
        else:
            pixels.append( int(f) )    
        linecounter += 1
    return pixels        

def encrypt(pixels, message):
    pos = 0 # first R pixel value reserved for message length
    num_bits = 8
    total_bits = len(message) * num_bits
    interval = (len( pixels) / 3) / total_bits

    pixels[pos] = len(message)
    pos += interval

    for i in range(len(message)):
        ascii = ord(message[i])                
        bits = [ascii >> bit & 1 for bit in range(num_bits - 1, -1, -1)]        
        
        for bit in bits:                        
            if bit is 1:
                pixels[pos] = pixels[pos] | 0b00000001
            elif bit is 0:
                pixels[pos] = pixels[pos] & 0b11111110
            #print('Stored at position : %i' % pos)
            pos += interval 

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-f", "--file", type=str, help="the image file to encrypt / decrypt")
    ap.add_argument( "-m", "--message", type=str, help="the secret message to encrypt")    
    ap.add_argument( "-o", "--output", type=str, help="the path to which the encrypted file should be saved")
    args = vars(ap.parse_args())

    # assign arguments to global variables
    file = open(args["file"])
    message = args["message"]    
    output = args["output"]
    
    if len(args["message"]) > 255:
        print("[ERROR] : message must be 255 characters or less")
        exit()    
    outputfile = []
    pixels = parsefile(file, outputfile)        
    encrypt(pixels, message)       
    pwd = raw_input("Set password: ")         
    encrypted = '# ' + hashlib.md5( pwd ).hexdigest()
    outputfile.append( encrypted )             
    for p in range( len( pixels ) ):
        outputfile.append(pixels[p])                    
    writefile(outputfile, output + "encrypted.ppm")                        
    print("[INFO] : Message successfully encrypted.  Image file output to : %s." % output )

if __name__ == "__main__":
    main()