# decrypt.py
# Description : decrypt a secret message in an image file.
# Author : Paul Sebeikin
# Date-created : 15 August 2016
# Date-modified : 12 September 2016

try:
    print "[INFO] Importing Libraries..."
    import argparse, numpy as np, hashlib
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

encrypted_pwd = None
minSz = 1
maxSz = 1000
pwdMinSz = 10
pwdMaxSz = 30

def parsefile(file, output):
    global encrypted_pwd
    headerlines = 4
    pixels = []
    linecounter = 0

    for f in file:     
        if linecounter < headerlines or str(f[0]) == "#" :
            output.append(f)            
        else:
            pixels.append( int(f) )    
        linecounter += 1
    return pixels        

def decrypt(pixels):
    decoded = ""
    pos = 0
    num_bits = 8    
    #print('Pixel length = %i, Interval = %i' % (len( pixels ), interval ) )
    binaryBuilder =  ['0b']

    length = pixels[pos]
    total_bits = length * num_bits
    interval = (len( pixels ) / 3) / total_bits
    pos += interval    

    while True:
        if len( decoded ) == length:
            break            
        val = pixels[pos]                
        bits = [val >> bit & 1 for bit in range(num_bits - 1, -1, -1)]        
        binaryBuilder.append(bits[-1])
        if len(binaryBuilder) == num_bits + 1: # + 1 because must take into account the 0b entry in the list           
            binary = ""
            for x in binaryBuilder:
                binary += str(x)
            ascii = int(binary, 2)
            decoded += chr(ascii)
            binaryBuilder = ['0b']
        #print('Found at position : %i, binary= %s' % (pos, binaryBuilder) ) 
        pos += interval
    return decoded

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-f", "--file", type=str, help="the image file to encrypt / decrypt")
    args = vars(ap.parse_args())

    # assign arguments to global variables
    file = open(args["file"])    
    

    outputfile = []
    pixels = parsefile(file, outputfile)
    pwd = raw_input("Enter password: ")
    orig_pwd = outputfile[4][2:-1]
    if hashlib.md5(pwd).hexdigest() == orig_pwd:
        output = decrypt(pixels)
        print("[INFO] : Message successfully decrypted: \nSecret Message = %s"  % output)
    else:
        print("Incorrect password")

if __name__ == "__main__":
    main()
