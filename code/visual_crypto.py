# visual_crypto.py
# Description : visually encrypt an image that can only be seen when a certain other image is overlayed.
# Author : Paul Sebeikin
# Date-created : 16 August 2016
# Date-modified : 22 August 2016

# packages
import cv2 as cv, numpy as np
import imutils, argparse, random, math

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-f", "--file", type=str, help="the source file")
    ap.add_argument( "-o", "--output", type=str, help="output path")
    args = vars(ap.parse_args())

    img = cv.imread(args['file'], 0)
    (h, w) = img.shape[:2]
    (thresh, img_bw) = cv.threshold(img, 128, 255, cv.THRESH_BINARY) # threshold(img, threshold, maxval, thresholdingMode)
    # img_bw = cv.cvtColor(img_bw, cv.COLOR_BGR2GRAY)
    # cv.imshow("img",img_bw)
    # cv.waitKey(0)
    # cv.imwrite('../images/test.png', img_bw)

    layer1 = np.zeros( (h*2, w*2) )
    layer2 = np.zeros( (h*2, w*2) )

    # print (len(layer1), len(layer1[0]))
    # print h,w

    posX = 0
    posY = 0
    random.seed()
    for i in range(len(img_bw)):
        for j in range(len(img_bw[i])):
            indx1 = random.randint(0,3)
            indx2 = random.randint(0,3)
            while indx1 == indx2:
                indx2 = random.randint(0,3)
            tmp = np.zeros(4)
            tmp[indx1] = 255 # change to white
            tmp[indx2] = 255 # change to white

            if img_bw[i, j] == 0: # black pixel
                for x in range(len(tmp)):
                    if x == 0 or x == 1:
                        #print "Attempting to insert %i at position %i:%i; x value is %i" % (tmp[x], posX, posY+x, x)
                        #print "Operating on black pixel %i:%i" % (i,j)
                        if (indx1 > 1):
                            layer1[posX,posY + x] = tmp[x]
                            layer2[posX,posY + x] = math.fabs(tmp[x] - 255)
                        else:
                            layer1[posX,posY + x] = math.fabs(tmp[x] - 255)
                            layer2[posX,posY + x] = tmp[x]
                    elif x == 2:
                        #print "Attempting to insert %i at position %i:%i; x value is %i" % (tmp[x], posX+1, posY, x)
                        #print "Operating on black pixel %i:%i" % (i,j)
                        if (indx1 > 1):
                            layer1[posX + 1,posY] = tmp[x]
                            layer2[posX + 1,posY] = math.fabs(tmp[x] - 255)
                        else:
                            layer1[posX + 1,posY] = math.fabs(tmp[x] - 255)
                            layer2[posX + 1,posY] = tmp[x]
                    elif x == 3:
                        #print "Attempting to insert %i at position %i:%i; x value is %i" % (tmp[x], posX+1, posY+1, x)
                        #print "Operating on black pixel %i:%i" % (i,j)
                        if (indx1 > 1):
                            layer1[posX + 1,posY + 1] = tmp[x]
                            layer2[posX + 1,posY + 1] = math.fabs(tmp[x] - 255)
                        else:
                            layer1[posX + 1,posY + 1] = math.fabs(tmp[x] - 255)
                            layer2[posX + 1,posY + 1] = tmp[x]
            elif img_bw[i, j] == 255:
                for x in range(0, len(tmp)):
                    if x == 0 or x == 1:
                        #print "Attempting to insert %i at position %i:%i; x value is %i" % (tmp[x], posX, posY+x, x)
                        #print "Operating on white pixel %i:%i" % (i,j)
                        layer1[posX,posY + x] = tmp[x]
                        layer2[posX,posY + x] = tmp[x]
                    elif x == 2:
                        #print "Attempting to insert %i at position %i:%i; x value is %i" % (tmp[x], posX+1, posY, x)
                        #print "Operating on white pixel %i:%i" % (i,j)
                        layer1[posX + 1,posY] = tmp[x]
                        layer2[posX + 1,posY] = tmp[x]
                    elif x == 3:
                        #print "Attempting to insert %i at position %i:%i; x value is %i" % (tmp[x], posX+1, posY+1, x)
                        #print "Operating on white pixel %i:%i" % (i,j)
                        layer1[posX + 1,posY + 1] = tmp[x]
                        layer2[posX + 1,posY + 1] = tmp[x]
            else:
                print "Not a black or white pixel : %i at position %i:%i" % (img_bw[i, j], i, j)
            posY += 2
            #print "Checking %i" % posY
            if (posY / len(layer1[0]) == 1):
                #print "Got to end of row x=%i and y=%i i=%i j=%i" % (posX, posY, i, j)
                posY = 0
                posX += 2

    cv.imwrite("../images/output/layer1.png", layer1)
    cv.imwrite("../images/output/layer2.png", layer2)
    cv.waitKey(0)
    cv.destroyAllWindows()

if __name__ == "__main__":
    main()
