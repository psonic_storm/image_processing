import sys, os, argparse

def main():
	ap = argparse.ArgumentParser()
	ap.add_argument( "-f", "--file", type=str, help="the file on which the operation should be performed" )	
	ap.add_argument( "-c", "--channel", type=str, help="the channel through which to perform a single channel greyscale operation")
	args = vars(ap.parse_args())

	file = open(args["file"])
	channel = args["channel"]

	linesRead = 0;
	greyscaled = list()
	fileParsed = os.path.splitext(args["file"])

	for line in file:
		try:
			if linesRead < 3:
				greyscaled.append(line)
			else:
				r, g, b = ( line, file.next(), file.next() )
				for i in range(3):
					if args["channel"] == 'r':
						greyscaled.append(r)
					elif args["channel"] == 'g':
						greyscaled.append(g)
					elif args["channel"] == 'b':
						greyscaled.append(b)
					else:
						print('Invalid algorithm option')
						quit()
			linesRead += 1
		except StopIteration:
			print('Finished reading file')

	output = fileParsed[0] + '_single_' + args["channel"] + '.ppm'
	f = open(output, 'w')
	for i in range(len(greyscaled)):
		f.write(str(greyscaled[i]) + '\n')
	f.close()
	print('File saved to: %s' % output)

if __name__ == "__main__":
	main()
