from distutils.core import setup
import py2exe, argparse

ap = argparse.ArgumentParser()
ap.add_argument( "-f", "--file", type=str, help="the file to compile to .exe")
args = vars(ap.parse_args())
  
setup(console=str(args["file"]))