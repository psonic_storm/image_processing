import sys, os, math

ppmPixels = []

class Pixel:
    def __init__(self, r, g, b):
        self.red = r
        self.green = g
        self.blue = b

def readFile(filename):
    return open(filename)

def getHeader(file):
    linesRead = 0
    temp = []
    for line in file:
        if linesRead < 3:
            temp.append(line)
        else:
            ppmPixels.append(line)
        linesRead += 1
    return temp

def getPixels(file):
    linesRead = 0
    pixels = list()
    for line in file:
        if linesRead == 0:
            p = Pixel(0, 0, 0)
            p.red = line
        elif linesRead  == 1:
            p.green = line
        elif linesRead == 2:
            p.blue = line
            pixels.append(p)
            linesRead = -1
        linesRead += 1
    return pixels

def interpolate(pixels, w1, h1, w2, h2):
    # initialize new image pixel list
    newPixels = [None] * (w2*h2)

    # work out scaling ratios
    x_ratio = (w1-1) / float(w2)
    y_ratio = (h1-1) / float(h2)

    offset = 0
    for i in range(h2):
        for j in range(w2):
            x = int(x_ratio * j)
            y = int(y_ratio * i)
            x_diff = int((x_ratio * j) - x)
            y_diff = int((y_ratio * i) - y)
            index = (y * w1) + x

            A = pixels[index]
            B = pixels[index+1]
            C = pixels[index+w1]
            D = pixels[index+w1+1]

            Ax = A.red * (1-x_diff) * (1-y_diff)
            Bx = B.red * x_diff * (1-y_diff)
            Cx = C.red * y_diff * (1-x_diff)
            Dx = D.red * (x_diff * y_diff)
            red = int(Ax + Bx + Cx + Dx)

            Ax = A.green * (1-x_diff) * (1-y_diff)
            Bx = B.green * x_diff * (1-y_diff)
            Cx = C.green * y_diff * (1-x_diff)
            Dx = D.green * (x_diff * y_diff)
            green = int(Ax + Bx + Cx + Dx)

            Ax = A.blue * (1-x_diff) * (1-y_diff)
            Bx = B.blue * x_diff * (1-y_diff)
            Cx = C.blue * y_diff * (1-x_diff)
            Dx = D.blue * (x_diff * y_diff)
            blue = int(Ax + Bx + Cx + Dx)

            newPixels[offset] = Pixel(red, green, blue)

            offset += 1
    return newPixels

def writeFile(contents, path):
    f = open(path, 'w')
    for i in contents:
        f.write(str(i) + '\n')
    f.close()
    print('File saved to: %s' % path)


def main():
    # get file contents
    file = readFile(sys.argv[1])
    temp = getHeader(file)
    pixels = getPixels(ppmPixels)

    print('Pixel List Length= ' + str(len(pixels)))

    # get original dimensions
    dimensions = temp[2].split(' ')
    # original image size
    w1 = int(dimensions[0])
    h1 = int(dimensions[1])

    # interpolated image size
    w2 = int(sys.argv[2])
    h2 = int(sys.argv[3])

    fileParsed = os.path.splitext(sys.argv[1])
    temp[2] = str(w2) + ' ' + str(h2) + '\n' # add new size to PPM file
    output = fileParsed[0] + '_' + 'interpolated' + fileParsed[1]

    # initiate interpolation
    newPixels = interpolate(pixels, w1, h1, w2, h2)

    for i in newPixels:
        temp.append(i.red)
        temp.append(i.green)
        temp.append(i.blue)

    writeFile(temp, output)

if __name__ == "__main__":
	main()
