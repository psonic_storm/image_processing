import sys, os, math, numpy as np

def affine_t(x, y, a, b, c, d, e, f):
    return round(a*x + b*y + e, 3), round(c*x + d*y + f, 3)

def mrotate(r, cx=0, cy=0):
    return (math.cos(r), -math.sin(r), math.sin(r), math.cos(r), cx, cy)

def rotate_coords(x, y, theta, ox, oy):
    s, c = np.sin(theta), np.cos(theta)
    x, y = np.asarray(x) - ox, np.asarray(y) - oy
    return x * c - y * s + ox, x * s + y * c + oy

def parsefile(file, outputFile):
    outputFile.append(file.next()) # P3
    outputFile.append(file.next()) # Comment
    outputFile.append(file.next()) # Dimentions

    # calculate new image size
    #dimensions = file.next().split(' ') # get width and height
    dimensions = outputFile[2].split(' ') # get width and height
    width = int(dimensions[0])
    height = int(dimensions[1])
    #outputFile.append( str(width*2) + ' ' + str(height*2) )
    outputFile.append(file.next()) # color thresholw (255)

    ndPixels = np.zeros( (width,height, 3), int ) # create 2d array

    for i in range(len(ndPixels)):
        for j in range(len(ndPixels[i])):
            for k in range(len(ndPixels[i][j])):
                ndPixels[i][j][k] = int(file.next())
    return ndPixels

def writeFile(contents, path):
    f = open(path, 'w')
    for i in contents:
        f.write(str(i) + '\n')
    f.close()
    print('File saved to: %s' % path)

def main():
    try:
        file = open(sys.argv[1]) # get file contents
    except:
        print("ERROR READING FILE PARAMETER")
        exit()

    outputfile = [] # will eventually be output to a file
    ndPixels = parsefile(file, outputfile) # multi-dimensional array of pixels

    # rotation angle
    try:
        angleDegrees = float(sys.argv[2])
    except:
        print("ERROR READING DEGREE PARAMETER")
        exit()

    angleRadians = math.radians(angleDegrees)
    cosAngle = math.cos(-angleRadians)
    sinAngle = math.sin(-angleRadians)

    originX, originY = 0,0

    origHeight, origWidth = len(ndPixels), len(ndPixels[0]) # source image dimensions
    rotatedX, rotatedY = rotate_coords([0, origWidth, origWidth, 0], [0, 0, origHeight, origHeight], -angleRadians, originX, originY)

    newWidth, newHeight  = (int( np.ceil(c.max() - c.min())) for c in (rotatedX, rotatedY))

    dx, dy = np.meshgrid(np.arange(newWidth), np.arange(newHeight))

    sx, sy = rotate_coords(dx + rotatedX.min(), dy + rotatedY.min(), -angleRadians, originX, originY)

    mask = (0 <= sx) & (sx < origWidth) & (0 <= sy) & (sy < origHeight)

    print(mask)
    PixelsRotated = np.empty(shape=(newHeight, newWidth))

    PixelsRotated[dy[mask], dx[mask]] = ndPixels[sy[mask], sx[mask]]

    PixelsRotated[dy[~mask], dx[~mask]] = 255

    '''newL = origWidth * math.cos(angleRadians)
    newR = origHeight * math.sin(angleRadians)
    nhL = origWidth * math.sin(angleRadians)
    nhU = origHeight * math.cos(angleRadians)

    newHeight, newWidth = int( math.ceil(newL + newR) ), int( math.ceil(nhL + nhU) )
    #originX, originY = origWidth/2.0 - 0.5, origHeight/2.0 - 0.5
    xOffset, yOffset = int( math.ceil((origWidth - newWidth)/2.0) ), int( math.ceil((origHeight - newHeight)/2.0))

    PixelsRotated = np.full( (newWidth, newHeight, 3), 255, int ) # make a copy of the new image
    print(len(PixelsRotated))
    print(len(PixelsRotated[0]))
    print(len(ndPixels))
    print(len(ndPixels[0]))

    for x in xrange(xOffset, newWidth):
        for y in xrange(yOffset, newHeight):
            ox, oy = affine_t(x - originX, y - originY, *mrotate(-angleRadians, originX, originY) )
            if ox > -1 and ox < origWidth and oy > -1 and oy < origHeight:
                print('x = %i and y = %i and ox = %i and oy = %i' % (x,y,ox,oy))
                PixelsRotated[x, y] = ndPixels[ox][oy]'''

    fileParsed = os.path.splitext(sys.argv[1])
    output = fileParsed[0] + '_rotated' + fileParsed[1]

    for i in range(len(PixelsRotated)):
        for j in range(len(PixelsRotated[i])):
            for k in range(len(PixelsRotated[i][j])):
                outputfile.append(int(PixelsRotated[j][i][k]))
    writeFile(outputfile, output)

if __name__ == "__main__":
	main()
