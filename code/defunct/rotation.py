import sys, os, math, numpy as np
ppmPixels = []

class Pixel:
    def __init__(self, r, g, b):
        self.red = r
        self.green = g
        self.blue = b

def readFile(filename):
    return open(filename)

def getHeader(file):
    linesRead = 0
    temp = []
    for line in file:
        if linesRead < 4:
            temp.append(line)
        else:
            ppmPixels.append(line)
        linesRead += 1
    return temp

def getPixels(ppm, w):
    linesRead = 0
    pixelsParsed = 0
    pixels = list()
    pixelRow = []
    for line in ppm:
        if linesRead == 0:
            p = Pixel(0, 0, 0)
            p.red = line
        elif linesRead  == 1:
            p.green = line
        elif linesRead == 2:
            p.blue = line
            pixelRow.append(p)
            linesRead = -1
            pixelsParsed += 1
            if pixelsParsed == w:
                #print('pixelsParsed = %i' % pixelsParsed)
                #print('length of pixelrow to be added = %i' % len(pixelRow))
                pixels.append(pixelRow)
                pixelRow = []
                pixelsParsed = 0
        linesRead += 1
    return pixels

def writeFile(contents, path):
    f = open(path, 'w')
    for i in contents:
        f.write(str(i))
    f.close()
    print('File saved to: %s' % path)

def main():
    try:
        file = readFile(sys.argv[1]) # get file contents
    except:
        print("NO FILE PARAMETER PROVIDED")
        break
    temp = getHeader(file) # get header contents

    # rotation angle
    try:
        angleDegrees = sys.argv[2]
    except:
        print("NO ANGLE PARAMETER PROVIDED")
        break
    angleRads = math.radians(angleDegrees)

    # get original dimensions
    dimensions = temp[2].split(' ')
    w = int(dimensions[0])
    h = int(dimensions[1])

    fileParsed = os.path.splitext(sys.argv[1])
    output = fileParsed[0] + '_' + 'rotated' + fileParsed[1]

    print('WIDTH = %s' % w)
    pixels = getPixels(ppmPixels, w)
    newPixels = pixels

    cosAngle = 0
    sinAngle = 1

    print(cosAngle)
    print(sinAngle)

    for i in range(len(pixels)):
        for j in range(len(pixels[i])):
            xA = cosAngle * (i - 0)
            xB = sinAngle * (j - 0)
            x = math.fabs(xA - xB  + 0)

            yA = sinAngle * (i - 0)
            yB = cosAngle * (j - 0)
            y = math.fabs(yA + yB + 0)

            #print('x coordinate = %i' % int(x))
            #print('y coordinate = %i\n' % int(y))
            #print('red = %s green = %s blue = %s' % (pixels[i][j].red, pixels[i][j].green, pixels[i][j].blue))
            newPixels[int(x)][int(y)] = pixels[i][j]

    for i in range(len(newPixels)):
        for j in range(len(newPixels[i])):
            temp.append(newPixels[i][j].red)
            temp.append(newPixels[i][j].green)
            temp.append(newPixels[i][j].blue)

    writeFile(temp, output)

if __name__ == "__main__":
	main()
