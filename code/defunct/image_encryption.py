# image_encryption.py
# Description : encrypt a secret message in an image file.  Decrypt the message by passing in the image and the length of the secret message
# Author : Paul Sebeikin
# Date-created : 10 August 2016
# Date-modified : 12 August 2016

# import packages
import cv2, argparse, time, numpy as np, imutils, os, hashlib

encrypted_pwd = None

def writefile(contents, path):
    f = open(path, 'w')
    for i in contents:
        if str(i)[-1:] == '\n':            
            f.write(str(i))
        else:            
            f.write(str(i) + '\n')
    f.close()

def parsefile(file, output):
    global encrypted_pwd
    headerlines = 4
    pixels = []
    linecounter = 0

    for f in file:     
        if linecounter < headerlines or str(f[0]) == "#" :
            output.append(f)            
        else:
            pixels.append( int(f) )    
        linecounter += 1
    return pixels        


def encrypt(pixels, message):
    interval = (len( pixels) / 3) / len( message )

    # Debugging    
    #print('Pixel length = %i, Interval = %i, Message Length = %i' % (len( pixels ), interval, len( message ) ) )
    pos = 0
    for i in range(len(message)):
        ascii = ord(message[i])        

        pixels[pos] = ascii

        #print('Stored at position : %i' % pos)
        pos += interval

def decrypt(pixels, length):
    decoded = ""
    pos = 0
    interval = (len( pixels ) / 3) / length
    #print('Pixel length = %i, Interval = %i' % (len( pixels ), interval ) )
    while True:
        if len( decoded ) == length:
            break
        letter = chr(int(pixels[pos]))  
        #print('Found at position : %i' % pos)      
        decoded += letter
        pos += interval
    return decoded

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-f", "--file", type=str, help="the image file to encrypt / decrypt")
    ap.add_argument( "-t", "--text", type=str, help="the secret message to encrypt")
    ap.add_argument( "-m", "--mode", type=int, help="modes : 0 - encrypt; 1 - decrypt")
    ap.add_argument( "-o", "--output", type=str, help="the path to which the encrypted file should be saved")
    ap.add_argument( "-l", "--length", type=int, help="length of the secret message.  Used to decrypt")
    args = vars(ap.parse_args())

    # assign arguments to global variables
    file = open(args["file"])
    message = args["text"]
    mode = args["mode"]
    output = args["output"]
    length = args["length"]    

    if mode is 0:
        outputfile = []
        pixels = parsefile(file, outputfile)        
        encrypt(pixels, message)                
        encrypted = '# ' + hashlib.md5( str(len( message)) ).hexdigest()
        outputfile.append( encrypted )             
        for p in range( len( pixels ) ):
            outputfile.append(pixels[p])                    
        writefile(outputfile, output)                        
        print("[INFO] : Message successfully encrypted.  Image file output to : %s. Password to unlock: %i" % (output, len(message) ) )

    elif mode is 1:        
        outputfile = []
        pixels = parsefile(file, outputfile)
        pwd = raw_input("Enter password: ")
        orig_pwd = outputfile[4][2:-1]
        if hashlib.md5(pwd).hexdigest() == orig_pwd:
            output = decrypt(pixels, int(pwd))
            print("Secret Message = %s"  % output)
        else:
            print("Incorrect password")

if __name__ == "__main__":
    main()
