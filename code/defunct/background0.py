import os, sys, fnmatch, numpy as np

def parsefile(file, outputFile):
    outputFile.append(file.next()) # P3
    outputFile.append(file.next()) # dimensions
    outputFile.append(file.next()) # color threshold (255)

    dimensions = outputFile[1].split(' ') # get width and height
    width = int(dimensions[0])
    height = int(dimensions[1])

    ndPixels = np.zeros( (width,height, 3) ) # create 2d array

    line = file.next()
    lineSplit = line.split(' ')

    if len(lineSplit) < 1:
        for i in range(len(ndPixels)):
            for j in range(len(ndPixels[i])):
                for k in range(len(ndPixels[i][j])):
                    if i == 0 & j == 0 & k == 0:
                        ndPixels[i][j][k] = int(line)
                    else:
                        ndPixels[i][j][k] = int(file.next())
    else:
        fileLineCounter = 0
        for i in range(len(ndPixels)):
            for j in range(len(ndPixels[i])):
                for k in range(len(ndPixels[i][j])):
                    ndPixels[i][j][k] = int(lineSplit[fileLineCounter])
                    fileLineCounter += 1
    return ndPixels

def writefile(contents, path):
    f = open(path, 'w')
    for i in contents:
        if str(i)[-1:] == '\n':
            f.write(str(i))
        else:
            f.write(str(i) + ' ')
    f.close()
    print('File saved to: %s' % path)

def movefile(file, dest):
    fileParsed = os.path.basename(file)
    dest = dest + fileParsed
    os.rename(file, dest)
    print('Processed file moved to: %s' % dest + fileParsed)

def background(file1, file2, threshold):
    result = np.zeros( (len(file1), len(file1[0]), 3) )
    for i in range(len(file1)):
        for j in range(len(file1[i])):
            if (file2[i][j][0] - file1[i][j][0] > threshold) | (file2[i][j][1] - file1[i][j][1] > threshold) | (file2[i][j][2] - file1[i][j][2] > threshold):
                    result[i][j][0] = 255
                    result[i][j][1] = 255
                    result[i][j][2] = 255
            else:
                result[i][j][0] = 0
                result[i][j][1] = 0
                result[i][j][2] = 0
    return result

def main():
    filePaths = []
    threshold = int(sys.argv[3])
    skipImages = int(sys.argv[4])
    for file in os.listdir(sys.argv[1]):
        if fnmatch.filter(os.listdir(sys.argv[1]), '*' + sys.argv[2]):
            filePaths.append(sys.argv[1] + '/' + file)
    i = 0
    fileCounter = 0
    while i < len(filePaths):
        outputFile1 = []
        outputFile2 = []
        pixelsFile1 = parsefile(open(filePaths[i]), outputFile1)
        pixelsFile2 = parsefile(open(filePaths[i+skipImages]), outputFile2)
        result =background(pixelsFile1, pixelsFile2, threshold)

        fileParsed = os.path.splitext(filePaths[i])
        output = '../images/subtraction/output/' + 'background_' + str(fileCounter) + fileParsed[1]

        for p in range(len(result)):
            for q in range(len(result[p])):
                for r in range(len(result[p][q])):
                    outputFile1.append(int(result[p][q][r]))
        writefile(outputFile1, output)
        movefile(filePaths[i], '../images/subtraction/processed/')
        movefile(filePaths[i+1], '../images/subtraction/processed/')
        fileCounter += 1
        i += skipImages + 1

if __name__ == "__main__":
	main()
