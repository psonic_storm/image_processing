def average(pixel):
	return (int(pixel[0]) + int(pixel[1]) + int(pixel[2])) / 3

im = Image.open("../images/flower.ppm")

for rowPixel in range(len(im)):
	for columnPixel in range(len(im[rowPixel])):
		greyscale[rowPixel][columnPixel] = average(im[rowPixel, columnPixel])

im.save('../images/greyscale/greyscale.jpeg')
