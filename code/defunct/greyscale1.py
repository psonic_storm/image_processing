from scipy import misc
import numpy as np
import sys

def greyscale_avg(image):
	greyscale = np.zeros((image.shape[0], image.shape[1]))
	for rowPixel in range(len(image)):
		for columnPixel in range(len(image[rowPixel])):
			greyscale[rowPixel][columnPixel] = average(image[rowPixel, columnPixel])
	createImage(greyscale, '../images/greyscale/greyscale.jpeg')

def greyscale_weighted_avg(image):
	greyscale = np.zeros((image.shape[0], image.shape[1]))
	for rowPixel in range(len(image)):
		for columnPixel in range(len(image[rowPixel])):
			greyscale[rowPixel][columnPixel] = weighted_avg(image[rowPixel, columnPixel])
	createImage(greyscale, '../images/greyscale/greyscale_weighted_avg.jpeg')

def getImage(filename):
	return misc.imread(filename)

def createImage(image, filename):
	misc.imsave(filename, image)
	
def average(pixel):
	return (int(pixel[0]) + int(pixel[1]) + int(pixel[2])) / 3

def weighted_avg(pixel):
	return 0.299*pixel[0] + 0.587*pixel[1] + 0.114*pixel[2]

def main():
	if len(sys.argv) < 3:
		print('Image argument required')
	else:
		image = getImage(sys.argv[1])
		if (int(sys.argv[2]) == 1):
			greyscale_avg(image)
			greyscale_weighted_avg(image)
		else:
			print('Invalid operation specified')

if __name__ == "__main__":
	main()
