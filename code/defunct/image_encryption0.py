# image_encryption.py
# Description : encrypt a secret message in an image file.  Decrypt the message by passing in the image and the length of the secret message
# Author : Paul Sebeikin
# Date-created : 12 August 2016
# Date-modified : 15 August 2016

# import packages
import cv2, argparse, time, numpy as np, imutils, os, hashlib, random

encrypted_pwd = None
minSz = 1
maxSz = 1000
pwdMinSz = 10
pwdMaxSz = 30

def writefile(contents, path):
    f = open(path, 'w')
    for i in contents:
        if str(i)[-1:] == '\n':            
            f.write(str(i))
        else:            
            f.write(str(i) + '\n')
    f.close()

def parsefile(file, output):
    global encrypted_pwd
    headerlines = 4
    pixels = []
    linecounter = 0

    for f in file:     
        if linecounter < headerlines or str(f[0]) == "#" :
            output.append(f)            
        else:
            pixels.append( int(f) )    
        linecounter += 1
    return pixels        

def encrypt(pixels, message):
    pos = 0 # first R pixel value reserved for message length
    num_bits = 8
    total_bits = len(message) * num_bits
    interval = (len( pixels) / 3) / total_bits

    pixels[pos] = len(message)
    pos += interval

    for i in range(len(message)):
        ascii = ord(message[i])                
        bits = [ascii >> bit & 1 for bit in range(num_bits - 1, -1, -1)]        
        
        for bit in bits:                        
            if bit is 1:
                pixels[pos] = pixels[pos] | 0b00000001
            elif bit is 0:
                pixels[pos] = pixels[pos] & 0b11111110
            #print('Stored at position : %i' % pos)
            pos += interval        

def decrypt(pixels):
    decoded = ""
    pos = 0
    num_bits = 8    
    #print('Pixel length = %i, Interval = %i' % (len( pixels ), interval ) )
    binaryBuilder =  ['0b']

    length = pixels[pos]
    total_bits = length * num_bits
    interval = (len( pixels ) / 3) / total_bits
    pos += interval

    

    while True:
        if len( decoded ) == length:
            break            
        val = pixels[pos]                
        bits = [val >> bit & 1 for bit in range(num_bits - 1, -1, -1)]        
        binaryBuilder.append(bits[-1])
        if len(binaryBuilder) == num_bits + 1: # + 1 because must take into account the 0b entry in the list           
            binary = ""
            for x in binaryBuilder:
                binary += str(x)
            ascii = int(binary, 2)
            decoded += chr(ascii)
            binaryBuilder = ['0b']
        #print('Found at position : %i, binary= %s' % (pos, binaryBuilder) ) 
        pos += interval
    return decoded

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-f", "--file", type=str, help="the image file to encrypt / decrypt")
    ap.add_argument( "-t", "--text", type=str, help="the secret message to encrypt")
    ap.add_argument( "-m", "--mode", type=int, help="modes : 0 - encrypt; 1 - decrypt")
    ap.add_argument( "-o", "--output", type=str, help="the path to which the encrypted file should be saved")
    args = vars(ap.parse_args())

    # assign arguments to global variables
    file = open(args["file"])
    message = args["text"]
    mode = args["mode"]
    output = args["output"]
    
    if mode is 0:
        if len(args["text"]) > 255:
            print("[ERROR] : message must be 255 characters or less")
            exit()    
        outputfile = []
        pixels = parsefile(file, outputfile)        
        encrypt(pixels, message)       
        pwd = raw_input("Set password: ")         
        encrypted = '# ' + hashlib.md5( pwd ).hexdigest()
        outputfile.append( encrypted )             
        for p in range( len( pixels ) ):
            outputfile.append(pixels[p])                    
        writefile(outputfile, output)                        
        print("[INFO] : Message successfully encrypted.  Image file output to : %s." % output )

    elif mode is 1:        
        outputfile = []
        pixels = parsefile(file, outputfile)
        pwd = raw_input("Enter password: ")
        orig_pwd = outputfile[4][2:-1]
        if hashlib.md5(pwd).hexdigest() == orig_pwd:
            output = decrypt(pixels)
            print("[INFO] : Message successfully decrypted: \n Secret Message = %s"  % output)
        else:
            print("Incorrect password")

if __name__ == "__main__":
    main()
