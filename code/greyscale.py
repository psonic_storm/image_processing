# greyscale.py
# Description : performs a greyscale operation using a specified algorithm on a given image
# Author : Paul Sebeikin
# Date-created : 21 July 2016
# Date-modified : 12 September 2016
try:
    print "[INFO] Importing Libraries..."
    import sys, os
    import argparse
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

def average(r, g, b):
	return (int(r) + int(g) + int(b)) / 3

def weighted_avg(r, g, b):
	return 0.299*float(r) + 0.587*float(g) + 0.114*float(b)

def main():
	ap = argparse.ArgumentParser()
	ap.add_argument( "-f", "--file", type=str, help="the file on which the operation should be performed" )
	ap.add_argument( "-o", "--output", type=str, help="output path")
	ap.add_argument( "-a", "--algorithm", type=int, default= 0, help="the algorithm to execute : 0 - greyscale average; 1 - greyscale weighted average; 2 - single channel")
	ap.add_argument( "-c", "--channel", type=str, help="the channel through which to perform a single channel greyscale operation")
	args = vars(ap.parse_args())

	file = open(args["file"])
	linesRead = 0;
	greyscaled = list()
	fileParsed = os.path.splitext(args["file"])
	operation = args["algorithm"]
	channel = args["channel"]
	output_path = args["output"]

	for line in file:
		try:
			if linesRead < 4:
				greyscaled.append(line) # save the header lines
			else:
				r,g,b = ( line, file.next(), file.next() )
				#print('r=%i, g=%i, b=%i' % (r,g,b) )
				val = None
				if operation == 0:
					val = average(r, g, b)
				elif operation == 1:
					val = int(weighted_avg(r, g, b))
				elif operation == 2:
					if channel == 'r':
						val = r
					elif channel == 'g':
						val = g
					elif channel == 'b':
						val = b					
				else:
					print('[ERROR] Invalid algorithm option')
					quit()
				for i in range(3):
					greyscaled.append(val)
			linesRead += 1
		except StopIteration:
			print('[INFO] Finished reading file')

	if operation == 0:
		output = output_path + 'greyscale_avg.ppm'
	elif operation == 1:
		output = output_path + 'greyscale_wavg.ppm'
	elif operation == 2:
		output = output_path + 'greyscale_single_' + channel + '.ppm'

	f = open(output, 'w')
	for i in range(len(greyscaled)):
		f.write(str(greyscaled[i]) + '\n')
	f.close()
	print('[INFO] File saved to: %s' % output)

if __name__ == "__main__":
	main()
