# rotation.py
# Description : performs a rotation using a specified angle on a given image
# Author : Paul Sebeikin
# Date-created : 27 July 2016
# Date-modified : 12 September 2016

try:
    print "[INFO] Importing Libraries..."
    import sys, os, math, numpy as np, argparse
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

def parsefile(file, outputFile):
    outputFile.append(file.next()) # P3
    outputFile.append(file.next()) # Comment
    outputFile.append(file.next()) # Dimentions
    dimensions = outputFile[2].split(' ') # get width and height
    width = int(dimensions[0])
    height = int(dimensions[1])
    outputFile.append(file.next()) # color threshold (255)

    res = np.zeros( (width,height, 3) ) # create 2d array

    for i in range(len(res)):
        for j in range(len(res[i])):
            for k in range(len(res[i][j])):
                res[i][j][k] = file.next()
    return res

def writeFile(contents, path):
    f = open(path, 'w')
    for i in contents:
        f.write(str(i) + '\n')
    f.close()
    print('File saved to: %s' % path)

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-f", "--file", type=str, help="the file on which the operation should be performed" )
    ap.add_argument( "-a", "--angle", type=int, help="the angle to rotate the image by")
    ap.add_argument( "-c", "--centre", nargs='*', default=(0,0), help="the center around which to rotate")
    ap.add_argument( "-o", "--output", type=str, help="output path")
    args = vars(ap.parse_args())
    try:
        file = open(args["file"]) # get file contents
    except:
        print("[ERROR] Could not read input file")
        exit()


    outputfile = [] # will eventually be output to a file
    src = parsefile(file, outputfile) # multi-dimensional array of pixels
    output_path = args["output"]
    width = len(src)
    height = len(src[0])
    centre = int(args["centre"][0]), int(args["centre"][1])

    print centre

    try: # rotation angle
        angleDegrees = float(args["angle"])
    except:
        print("[ERROR] Could not read angle")
        exit()

    angleRadians = math.radians(angleDegrees)
    cosine = float(math.cos(angleRadians))
    sine = float(math.sin(angleRadians))

    # print('cosine= %f \t sine= %f ' % (cosine, sine))

    x1 = int(-height * sine)
    y1 = int(height * cosine)
    x2 = int(width * cosine - height * sine)
    y2 = int(height * cosine + width * sine)
    x3 = int(width * cosine)
    y3 = int(width * sine)

    minx = min(0, min(x1, min(x2, x3)))
    miny = min(0, min(y1, min(y2, y3)))
    maxx = max(0, max(x1, max(x2, x3)))
    maxy = max(0, max(y1, max(y2, y3)))

    w = maxx - minx
    h = maxy - miny

    dest = np.full( (w, h, 3), 255, int ) # the new image resized to take into account the rotation

    output = output_path + 'rotated.ppm'

    outputfile[2] = str(w) + ' ' + str(h) + '\n' # add new size to PPM file

    for y in range(miny, maxy):
        for x in range(minx, maxx):
            sourceX = int((x-centre[0]) * cosine + (y-centre[1]) * sine)
            sourceY = int((y-centre[1]) * cosine - (x-centre[0]) * sine)
            if sourceX >= 0 and sourceX < width and sourceY >= 0 and sourceY < height:
                dest[x,y] = src[sourceY, sourceX]

            #x = int( (cosAngle * (i - originX)) - (sinAngle * (j - originY)) )
            #y = int( (sinAngle * (i - originX)) + (cosAngle * (j - originY)) )

            #print('sin=%f cos=%f x=%i y=%i' % (sinAngle,cosAngle, x,y,))
            #print('set %i to %i' %  (PixelsRotated[x][y], ndPixels[i][j]) )
    for i in range(len(dest)):
        for j in range(len(dest[i])):
            for k in range(len(dest[i][j])):
                outputfile.append(dest[j][i][k])
    writeFile(outputfile, output)

if __name__ == "__main__":
	main()
