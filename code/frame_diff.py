# frame_diff.py
# Description : performs a frame difference operation on a set of PPM images in a specified directory
# Author : Paul Sebeikin
# Date-created : 27 July 2016
# Date-modified : 12 September 2016

try:
    print "[INFO] Importing Libraries..."
    import os, sys, fnmatch, numpy as np, argparse, math
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

def parsefile(file, outputFile):
    outputFile.append(file.next()) # P3
    outputFile.append(file.next()) # Comment
    outputFile.append(file.next()) # dimensions
    outputFile.append(file.next()) # color thresholw (255)

    dimensions = outputFile[2].split(' ') # get width and height

    width = int(dimensions[0])
    height = int(dimensions[1])

    ndPixels = np.zeros( (width,height, 3) ) # create 2d array

    for i in range(len(ndPixels)):
        for j in range(len(ndPixels[i])):
            for k in range(len(ndPixels[i][j])):
                ndPixels[i][j][k] = file.next()
    return ndPixels

def writefile(contents, path):
    f = open(path, 'w')
    for i in contents:
        f.write(str(i) + '\n')
    f.close()
    print('[INFO] File saved to: %s' % path)

def movefile(file, dest):
    fileParsed = os.path.basename(file)
    print(file)
    print (dest + fileParsed)
    dest = dest + fileParsed
    os.rename(file, dest)
    print('Processed file moved to: %s' % dest + fileParsed)

def background(file1, file2, threshold):
    result = np.zeros( (len(file1), len(file1[0]), 3) )
    for i in range(len(file1)):
        for j in range(len(file1[i])):
            if (math.fabs(file2[i][j][0] - file1[i][j][0]) + math.fabs(file2[i][j][1] - file1[i][j][1]) + math.fabs(file2[i][j][2] - file1[i][j][2])) >= threshold:
                result[i][j][0] = 255
                result[i][j][1] = 255
                result[i][j][2] = 255
            else:
                result[i][j][0] = 0
                result[i][j][1] = 0
                result[i][j][2] = 0
    return result

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-o", "--output", type=str, help="output path")
    ap.add_argument( "-d", "--dir", type=str, help="directory in which to find ppm files")
    ap.add_argument( "-t", "--thresh", type=int, default=50, help="the threshold value")
    args = vars(ap.parse_args())

    filePaths = []
    threshold = args["thresh"]
    directory = args["dir"]
    output_path = args["output"]
    for file in os.listdir(directory):
        filePaths.append(directory + '/' + file)
    i = 0
    print "[INFO] Starting frame difference operation"
    while i < len(filePaths) - 1:
        outputFile1 = []
        outputFile2 = []
        pixelsFile1 = parsefile(open(filePaths[i]), outputFile1)
        pixelsFile2 = parsefile(open(filePaths[i+1]), outputFile2)
        result = background(pixelsFile1, pixelsFile2, threshold)

        fileParsed = os.path.splitext(filePaths[i])
        output = output_path + 'frame_diff_' + str(i) + fileParsed[1]

        for p in range(len(result)):
            for q in range(len(result[p])):
                for r in range(len(result[p][q])):
                    outputFile1.append(int(result[p][q][r]))
        writefile(outputFile1, output)
        # movefile(filePaths[i], '../images/subtraction/processed/')
        # movefile(filePaths[i+1], '../images/subtraction/processed/')
        i +=1

if __name__ == "__main__":
	main()
