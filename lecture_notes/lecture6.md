## Lecture 6 - 27/07/2016

- Histograms provide the frequency of pixel values in an image.
- Number of pixels of each value that occur in that image
- Sometime displayed as ration but usually as a graph.
- `Negative` : 1 - pixel value
- **Stretching** (Histogram Equalization) ~ technique used to stretch the frequency distribution of the pixel range across a broader swath of colors.
- **Histogram Back Projection** ~ all pixels that do not occur in the sample image's histogram, make that pixel black.  This is a technique that can be used to detect objects.
- `Masking` ~ place image that has been changed by some image processing technique and place it over the original image to get back some or all of the original colors.
- **Camshift** ~ based on `Mean-shift`.  Detects maxima of density function. Color-based tracking system.
- Camshift is developed to work better with videos.  
