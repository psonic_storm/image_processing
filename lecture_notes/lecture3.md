## Lecture 2 - 20/07/2016

- **Filter** ~ changes an image in some way
- *High-pass* and *Low-pass* filters
- Low-pass filters make images blurry whilc high-pass filters make them sharper.
- **Median-Filter** ~ arrange cells in ascending/descending order and find the middle cell.  Keep that value.
- Often used to remove `noise`.
- Lots of different types of noise and different filters are useful for different kinds of noise.
- **Mean Filter**
- **Gaussian smoothing**
- Unsharp masking is a nice way of clearing up images/
