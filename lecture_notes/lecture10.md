## Lecture 10 - 05/08/2016

### Video Stabilization
- Optical Flow ~ find out the direction of motion
- Feature Detection
- Compensate for motion
- Viola-Jones
- Cam Shift / Mean Shift
- Background Subtraction
- Frame differencing
- What is the difference between Background Subtraction and Frame Differencing?
  -
- **Homography** ~ feature detection
- **Planar** ~ draws plans around regions of interest (features)
- GoodFeaturesToTrack ~ OpenCV
- Seaside by Lyvo
