## Lecture 1 - 18/7/2016

- Sight is something that we can inherently **do** but also something that we need to **learn**.  Example: learn what a shape is vs. seeing the shape.
- `DPI` : Dots per inch
- **PBM Format** ~ format of 6 formats for representing pictures.
- **PGM** ~ *Greyscale* image.  Maximum bit number is 15.
- **RGB Format** ~ using 255 colors and RGB triplets.  An image in P3 is generally 3 times as big as images in P6.
- Videos are sets of images so video and image processing are largely the same area of study.
- 30% Red + 59% Green + 11% Blue = **Grey**
- We will be looking at **image differencing**.
- Image differencing is done by subtracting corresponding values from two different images and seeing whether there is a difference.
- **Edge detection** ~ run a filter and run over an image.  Filter will amplify different.  Amplifies areas of an images where there is **rapid color change**.  These are called the `edges` of the image.
- You also get multi-colored edges.
- Meet challenges of damaged fingerprints
s
### Task for Tomorrow
- Create image in a text editor
- Scale the image
- Greyscale the image
- Rotate the image
- Background subtraction.
