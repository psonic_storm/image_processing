## Lecture 7 - 28/07/2016

- Prepare images in some way that they are easier to work with such as **bar-codes**, **QR codes** etc.
- 32 different standards of bar codes
- 2 Dimensional bar code ~ **PDF417** : portable data file
  - Each work made out of 17 bits
  - Contains four bars and spaces
  - each pattern is 17 units long
  - 4 black zones, 4 white zones : always
- Watch video about guy decoding PDF417 (`EXAM!!!`)
- `QR Codes`
  - Error Correction ~ can destroy part of the QR code and still be able to read it.
  - There are several levels of error correction.
  - As you increase the percentage of error correction you can store less and less data in the QR code.
  - QR codes come in different versions of varying sizes.
  - Bigger and can put more information on it.
  - Encoding block tells us whether the data is numeric, alphanumeric etc.
