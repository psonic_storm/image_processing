## Lecture 8 - 01/08/2016

- Visual Cryptography (**Steganography**)
- As old as 1994.
- Division of information across a number of people.  All people need to be present to unlock messages.
- Process:
  - Convert image to black and white
  - white pixel : 50% black and 50% white
  - Black pixel must result in a black pixel when combined.
- If working with color pixel:
  - Divide pixel into four sub-pixels
  -
