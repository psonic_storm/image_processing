# JC Practical Lecture

- Numpy used for array management.
- CV::Mat is the C++ equivalent.
- Python, C# and C++ always compiled down to C.
- Open a picture ~ `cv2.imread(filepath, CONVERSION_ALGORITHM)`
- View picture ~ `cv2.imshow(windowname, image)`
- `cv2.waitKey(0)` ~ interrupt to keep image on the screen.
- Resize image ~ `cv2.resize(image, (width, height), 0, 0, interpolation = method)`
  - `cv2.resize(src, dsize[, dst[, fx[, fy[, interpolation]]]]) → dst`
  - fx and fy are horizontal and vertical scaling factors.  
- `cv2.cvtColor` ~ used to changed from one color space to any other.
- 
