# Image  Processing - Exam Question
Author : Paul Sebeikin
Date : 14/09/2016

Ideas:
1. What is the difference between a high-pass and low-pass filter? (5)
2. What effect(s) will increasing the error correction level have on a QR code? (4)
3. Create a Huffman tree using the frequency table below and decode the following message:

|Symbol|Frequency|
|:-----|:--------|
|a|32|
|s|22|
|o|20|
|m|19|
|n|18|
|p|15|
|r|12|
|c|11|
|e|10|
|g|5|
|i|4|






4. What is a histogram and how can it be used to detect objects in an image? (5)
